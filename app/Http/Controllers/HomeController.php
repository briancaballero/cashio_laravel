<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::get_users(false, 5);
        return view('myHome')->with('users', $users);
    }


    /**
     * Show the my users page.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function myUsers()
    {
        return view('myUsers');
    } */
}
