<?php

namespace App\Http\Controllers;

use App\Transactions;
use App\Uploads;
use Illuminate\Http\Request;
use Response;
use Session;
use App\Service\CommissionServiceInterface;

class TransactionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filename = $request->session()->pull('csvfilename');
        return view('myTransactions')->with('filename', $filename);;
    }

    /**
     * This method call on <form > submit.
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadTransactions(Request $request, CommissionServiceInterface $commission)
    {
        if ($file = $request->file('file'))
        {
            $filename   = $file->getClientOriginalName();
            $extension  = $file->getClientOriginalExtension();
            $tempPath   = $file->getRealPath();
            $fileSize   = $file->getSize();
            $mimeType   = $file->getMimeType();
        
            // Valid File Extensions
            $valid_extension = array("csv");
        
            // 2MB in Bytes
            $maxFileSize = 2097152;
            
            // Check file extension
            if(in_array(strtolower($extension),$valid_extension)){
    
                // Check file size
                if($fileSize <= $maxFileSize)
                {
                    
                    $location = 'uploads';
                    
                    $file->move($location,$filename);
                    $filepath = public_path($location."/".$filename);
        
                    $file = fopen($filepath,"r");
                    $importData_arr = array();
                    
                    $i = 0;
                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE)
                    {
                        $num = count($filedata );
                    
                        // Skip first row (Remove below comment if you want to skip the first row)
                        /*if($i == 0)
                        {
                            $i++;
                            continue; 
                        }*/
                        
                        for ($c=0; $c < $num; $c++)
                        {
                            $importData_arr[$i][] = $filedata [$c];
                        }
                        
                        $i++;
                    }
                    
                    fclose($file);

                    $upload = new Uploads;
                    $upload->filename = $filename;
                    $upload->save();

                    // Insert to MySQL database
                    foreach($importData_arr as $importData)
                    {
                        // validate data
                        if (count($importData) != 6)
                        {
                            Session::flash('message','Invalid CSV format.');
                            return redirect()->route('transactions');
                        }

                        if (!is_numeric($importData[4]))
                        {
                            continue;
                        }

                        if ($importData[4] < 0)
                        {
                            continue;
                        }

                        $commission->processCommission($upload->id, $importData);
                    }

                    $commission->reprocessCommission($upload->id);

                    $request->session()->put('csvfilename', $filename);
                    
                    Session::flash('message','Import Successful.');
                }
                else
                {
                    Session::flash('message','File too large. File must be less than 2MB.');
                }
    
            }
            else
            {
                Session::flash('message','Invalid File Extension.');
            }
        }
        else
        {
            Session::flash('message','CSV File is required.');
        }

        // ToDO: Process cashout of natural persons

        return redirect()->route('transactions');
    }

    public function exportTransactions(Request $request)
    {
        $request->validate([
            'filename' => 'required'
        ]);

        $filename = $request->input('filename');

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=$filename",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $recentupload = Uploads::orderBy('id', 'desc')->first();
        $transactions = Transactions::where('uploadid', $recentupload->id)->get();
        
        $callback = function() use ($transactions)
        {
            $file = fopen('php://output', 'w');
            
            foreach($transactions as $transaction) {
                fputcsv($file, array(number_format($transaction->commission,2)));
            }

            fclose($file);
        };

        return Response::stream($callback, 200, $headers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function show(Transactions $transactions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function edit(Transactions $transactions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transactions $transactions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transactions $transactions)
    {
        //
    }
}
