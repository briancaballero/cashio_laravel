<?php

namespace App\Service;

use App\Transactions;

class CommissionService implements CommissionServiceInterface
{
    public function processCommission($uploadid, $transactiondata)
    {
        try
        {
            $operationdate     = $transactiondata[0];
            $userid            = $transactiondata[1];
            $usertype          = $transactiondata[2];
            $operationtype     = $transactiondata[3];
            $operationamount   = $transactiondata[4];
            $operationcurrency = $transactiondata[5];
        }
        catch (Exception $e)
        {
            return false;
        }

        if ($operationtype == 'cash_in')
        {
            $commission = ($operationamount * 0.0003);

            if ($commission > 5)
            {
                $commission = 5.00;
            }
        }
        else
        {
            if ($usertype == 'legal')
            {
                $commission = ($operationamount * 0.003);

                if ($commission < 0.50)
                {
                    $commission = 0.50;
                }
            }
            else
            {
                if ($operationamount > 1000)
                {
                    $commission = (($operationamount % 1000) * 0.003);
                }
                else
                {
                    $commission = ($operationamount * 0.003);
                }
            }
        }

        $transaction = new Transactions;
        $transaction->uploadid          = $uploadid;
        $transaction->operationdate     = $operationdate;
        $transaction->userid            = $userid;
        $transaction->usertype          = $usertype;
        $transaction->operationtype     = $operationtype;
        $transaction->operationamount   = $operationamount;
        $transaction->operationcurrency = $operationcurrency;
        $transaction->commission        = $commission;

        $transaction->save();

        return true;
    }

    public function reprocessCommission($uploadid)
    {
        $transaction_total_per_user = array();
        $transactions = Transactions::where('uploadid', $uploadid)
                        ->where('operationtype', 'cash_out')
                        ->where('usertype', 'natural')
                        ->orderBy('userid', 'ASC')
                        ->orderBy('operationdate', 'ASC')
                        ->get();
        
        foreach ($transactions as $transaction)
        {
            if (!isset($transaction_total_per_user[$transaction->userid]))
            {
                $transaction_total_per_user[$transaction->userid] = array();
            }

            list($week_start, $week_end) = $this->week_range($transaction->operationdate);
            $weekkey = $week_start . '_' . $week_end;

            if (!isset($transaction_total_per_user[$transaction->userid][$weekkey]))
            {
                $transaction_total_per_user[$transaction->userid][$weekkey] = 0;
            }
            
            $week_amount = $transaction_total_per_user[$transaction->userid][$weekkey];

            if ($transaction->operationcurrency != 'EUR')
            {
                $operationcurrency = $this->convert_eur($transaction->operationamount, $transaction->operationcurrency);
            }
            else{
                $operationcurrency = $transaction->operationamount;
            }

            $excess_eur = ($week_amount + $operationcurrency) - 1000;

            if ($excess_eur > 0)
            {
                $commission = ($excess_eur * 0.003);
            }
            else
            {
                $commission = 0;
                $transaction_total_per_user[$transaction->userid][$weekkey] += $operationcurrency;
            }

            $updatecommission = Transactions::find($transaction->id); 
            $updatecommission->commission = $commission;
            $updatecommission->save();

        }
    }

    private function week_range($date)
    {
        $ts = strtotime($date);
        $start = (date('w', $ts) == 0) ? $ts : strtotime('last monday', $ts);
        return array(date('Y-m-d', $start),
                     date('Y-m-d', strtotime('next sunday', $start)));
    }

    private function convert_eur($amount, $currency)
    {
        switch ($currency) {
            case 'EUR':
                return $amount;
                break;
            case 'USD':
                return $amount * 0.89;
                break;
            case 'JPY':
                return $amount * 0.0084;
                break;
            default:
                return 0;
                break;
        }
    }
}