<?php

namespace App\Service;

interface CommissionServiceInterface
{
    public function processCommission($uploadid, $transactiondata);
}