<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    protected $table = 'transactions';

    protected $fillable = [
        'operationdate',
        'userid',
        'usertype',
        'operationtype',
        'operationamount',
        'operationcurrency',
        'commission'
    ];

    public $timestamps = false;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        self::creating(function ($transaction)
        {
            if ($transaction->operationtype == 'cash_in')
            {
                $transaction->commission = ($transaction->operationamount * 0.0003);

                if ($transaction->commission > 5)
                {
                    $transaction->commission = 5.00;
                }
            }
            else
            {
                if ($transaction->usertype == 'legal')
                {
                    $transaction->commission = ($transaction->operationamount * 0.003);

                    if ($transaction->commission < 0.50)
                    {
                        $transaction->commission = 0.50;
                    }
                }
                else
                {
                    if ($transaction->operationamount > 1000)
                    {
                        $transaction->commission = (($transaction->operationamount % 1000) * 0.003);
                    }
                    else
                    {
                        $transaction->commission = ($transaction->operationamount * 0.003);
                    }
                    
                }
            }
        });
    }
    
}
