<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Brian Caballero',
            'email' => 'brian.caballero1204@gmail.com',
            'password' => bcrypt('qwaszx1234'),
        ]);
        DB::table('users')->insert([
            'name' => 'Another User',
            'email' => 'another.user@testemail.com',
            'password' => bcrypt('qwaszx1234'),
        ]);
    }
}
