@extends('theme.default')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="overview-wrap">
                <h2 class="title-1">{{ __('Welcome') }} {{ Auth::user()->name }}</h2>
                </div>
            </div>
        </div>
        <div class="row m-t-25">
            <div class="col-lg-12">
                <h2 class="title-1 m-b-25">{{ __('Users') }}</h2>
                <div class="table-responsive table--no-card m-b-40">
                <table class="table table-borderless table-striped table-earning">
                    <thead>
                        <tr>
                            <th>{{ __('User ID') }}</th>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Email') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                        </tr>
                        @empty
                        <tr><td colspan="3">{{ __('No Users') }}</td></tr>
                        @endforelse
                        @if (count($users) >= 5)
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><a href="#">{{ __('More') }}</a></td>
                        </tr>
                        @endif
                    </tbody>
                </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="copyright">
                <p>Copyright © 2019 Colorlib. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
@endsection