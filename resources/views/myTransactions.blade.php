@extends('theme.default')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="overview-wrap">
                <h2 class="title-1">{{ __('Transactions') }}</h2>
                </div>
            </div>
        </div>
        <div class="row m-t-25">
            <div class="col-lg-12">
                @if(Session::has('message'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif
                @if (!empty($filename))
                    <form method="post" id="csv-export" action="{{ route('exporttransactions') }}" enctype="multipart/form-data" >
                        @csrf
                        <input type="hidden" name="filename" value="{{ $filename }}">
                    </form>
                    <script type="text/javascript">
                        document.getElementById('csv-export').submit();
                    </script>
                @endif

                <div class="card">
                    <div class="card-header">
                        {{ __('Import CSV`') }}
                    </div>
                    <div class="card-body card-block">
                        <form method="post" id="csv-upload" action="{{ route('uploadtransactions') }}" enctype="multipart/form-data" >
                            @csrf
                            <input type='file' name='file' >
                        </form>
                    </div>
                    <div class="card-footer">
                    
                        <button type="button" class="btn btn-primary" onclick="document.getElementById('csv-upload').submit();"><i class="fa fa-dot-circle-o"></i> {{ __('Import') }}</button>
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@endsection