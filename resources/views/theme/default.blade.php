
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('theme.head')

<body class="animsition">
<div class="page-wrapper">
    @include('theme.headermobile')
    @include('theme.sidebar')
    <div class="page-container">
        @include('theme.header')
        <div class="main-content">
            <div class="section__content section__content--p30">
                @yield('content')
            </div>
        </div>
    </div>
</div>

@include('theme.footer-scripts')
</body>
</html>
