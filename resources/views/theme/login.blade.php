<!DOCTYPE html>
<html lang="en">
    @include('theme.head')
    <body class="animsition">
        <div class="page-wrapper">
            <div class="page-content--bge5">
                <div class="container">
                    <div class="login-wrap">
                        <div class="login-content">
                            <div class="login-logo">
                                <a href="#">
                                <img src="images/icon/logo.png" alt="CoolAdmin">
                                </a>
                            </div>
                            <div class="login-form">
                                @yield('content')
                                <!-- <div class="register-link">
                                    <p>
                                        Don't you have account?
                                        <a href="#">Sign Up Here</a>
                                    </p>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('theme.footer-scripts')
    </body>
</html>