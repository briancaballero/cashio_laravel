<aside class="menu-sidebar d-none d-lg-block">
   <div class="logo">
      <a href="#">
      <img src="images/icon/logo.png" alt="Cool Admin" />
      </a>
   </div>
   <div class="menu-sidebar__content js-scrollbar1">
      <nav class="navbar-sidebar">
         <ul class="list-unstyled navbar__list">
            <li class="{{ (Route::currentRouteName() == 'home') ? 'active' : '' }}">
               <a class="js-arrow" href="{{ route('home') }}">
               <i class="fas fa-tachometer-alt"></i>{{ __('Dashboard') }}</a>
            </li>
            <li class="{{ (Route::currentRouteName() == 'transactions') ? 'active' : '' }}">
               <a class="js-arrow" href="{{ route('transactions') }}">
               <i class="fas fa-tachometer-alt"></i>{{ __('Transactions') }}</a>
            </li>
         </ul>
      </nav>
   </div>
</aside>