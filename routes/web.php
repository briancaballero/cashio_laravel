<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/bootstrap', function () {
    return view('bootstrap');
});

Route::get('my-home', 'HomeController@myHome');
Route::get('my-users', 'HomeController@myUsers');
// Route::get('login', 'HomeController@login');

Auth::routes();

Route::get('login', 'Auth\LoginController@showLoginPage')->name('login');
Route::post('login', 'Auth\LoginController@loginNameOrEmail');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/', 'HomeController@index');

    Route::get('transactions', 'TransactionsController@index')->name('transactions');
    Route::post('uploadtransactions', 'TransactionsController@uploadTransactions')->name('uploadtransactions');
    Route::post('exporttransactions', 'TransactionsController@exportTransactions')->name('exporttransactions');
});



// Route::get('/', 'HomeController@index');
