<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class CommissionProcessTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testUpload()
    {
        Storage::fake('csv');

        $file = UploadedFile::fake()->csv('test.csv');

        $response = $this->json('POST', '/transactions', [
            'file' => $file,
        ]);

        Storage::disk('file')->assertExists($file->hashName());

        Storage::disk('file')->assertMissing('missing.csv');
    }
}
